# Containerized Rust Actix Web Service

This project contains a simple Rust Actix web service that serves static HTML files and provides a dynamic endpoint to generate random greetings. The web service is containerized using Docker for easy deployment and scaling.

## Getting Started

These instructions will cover how to build a Docker image for the Rust Actix web service and run it locally as a container.

### Prerequisites

- Docker
- Git (optional, for cloning the repository)

### Building the Docker Image

1. Clone the repository (if available) or ensure that the `Dockerfile`, `main.rs`, and the `static` directory containing `index.html` and `greeting.html` are in the current working directory.

    ```sh
    git clone <my repo url>
    cd <my repo name>
    ```

2. Build the Docker image with the following command:

    ```sh
    docker build -t <you can name it whatever you want> .
    ```

    This will compile the Rust application and create a Docker image based on your naming fashion.

### Running the Container

After building the image, you can run the container locally using:

```sh
docker run -d -p 50505:50505 <the docker image name you named>

```

### Results Preview
- **My App Local Image**:

![Alt text](img/image.png)
![Alt text](img/image-1.png)


- **Docker Image**:

![Alt text](img/image-2.png)
![Alt text](img/image-3.png)
