use actix_files::Files;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;

// Function to return a random greeting
async fn random_greeting() -> impl Responder {
    let greetings = vec![
        "Hello, world!",
        "Hi there!",
        "Good day!",
        "Bonjour!",
        "Hola!",
        "Guten Tag!",
        "こんにちは (Konnichiwa)!",
        "안녕하세요 (Annyeonghaseyo)!",
        "Привет (Privet)!",
        "Ciao!",
        "你好 (Nihao)",
    ];
    let mut rng = rand::thread_rng();
    let greeting = greetings[rng.gen_range(0..greetings.len())];
    HttpResponse::Ok().body(greeting)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            // Route to the random_greeting function
            .route("/random_greeting", web::get().to(random_greeting))
            // Serve static files from the directory "./static/root" and use "index.html" as the index file
            .service(Files::new("/", "./static/root/").index_file("index.html"))
    })
    .bind(("0.0.0.0", 50505))? // Listen on all interfaces at port 50505
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use actix_web::{test, web, App, Error};

    use super::*;

    #[actix_web::test]
    async fn test_random_greeting() -> Result<(), Error> {
        let app = App::new().route("/random_greeting", web::get().to(random_greeting));
        let app = test::init_service(app).await;

        let req = test::TestRequest::get()
            .uri("/random_greeting")
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert!(resp.status().is_success());
        Ok(())
    }
}
